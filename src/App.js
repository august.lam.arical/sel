import React from "react";
import * as d3 from "d3";
import testData from "./test.csv";
import testData1 from "./test1.csv";
import testData2 from "./test2.csv";

// set the dimensions and margins of the graph
const margin = { top: 10, right: 30, bottom: 30, left: 40 },
  width = 960 - margin.left - margin.right,
  height = 500 - margin.top - margin.bottom;
var bound = [0, 12000000];

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    // append the svg object to the body of the page
    const svg = d3.select("#my_dataviz")
      .append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", `translate(${margin.left},${margin.top})`);
    // X axis: scale and draw:
    const x = d3.scaleLinear()
      .domain([0, 12000000])     // can use this instead of 1000 to have the max of data: d3.max(data, function(d) { return +d.price })
      .range([0, width]);
    //add text
    svg.append("text")
      .attr("x", width * (bound[0] / 12000000) - (Math.round(bound[0]).toString().length) * 3.75)
      .attr("y", 0)
      .attr("id", "l")
      .text(bound[0])
      .style('fill', 'blue').style('font-size', '12px').style('visibility', 'hidden').style('height', '14px');
    svg.append("text")
      .attr("x", width * (bound[0] / 12000000) - (Math.round(bound[0]).toString().length) * 3.75)
      .attr("y", 0)
      .attr("id", "r")
      .text(bound[1])
      .style('fill', 'blue').style('font-size', '12px').style('visibility', 'hidden').style('height', '14px');

    const xAxis = d3.axisBottom(x);
    xAxis.tickValues([0, 2000000, 4000000, 6000000, 8000000, 10000000, 12000000]);
    xAxis.ticks().tickFormat(function (d) {
      return d >= 12000000 ? "12.0M+" : d !== 0 ? d / 1000000 + ".0M" : 0;
    });
    svg.append("g");

    // set the parameters for the histogram
    const histogram = d3.histogram()
      .value(function (d) { return d.price >= 12000000 ? 12000000 - 1 : d.price; })   // I need to give the vector of value
      .domain([0, 20000000])  // then the domain of the graphic
      .thresholds(x.ticks(7)); // then the numbers of bins


    // Y axis: initialization
    const y = d3.scaleLinear()
      .range([height, 0]);
    const yAxis = svg.append("g")

    d3.select("#dataButton").on("change", function (event) {
      d3.csv(getData(event.target.value)).then(function (data) {
        console.log(data);

        //reload data
        svg.select("g")
          .attr("transform", `translate(0, ${height})`)
          .call(xAxis);

        // And apply this function to data to get the bins
        const bins = histogram(data);
        // console.log(bins, data);

        // Y axis: update now that we know the domain
        y.domain([0, d3.max(bins, function (d) { return d.length; })]);   // d3.hist has to be called before the Y axis obviously
        // svg.append("g")
        yAxis.call(d3.axisLeft(y));

        // append the bar rectangles to the svg element
        svg.selectAll("rect")
          .data(bins)
          .join("rect")
          .attr("x", 1)
          .attr("transform", function (d) { return `translate(${x(d.x0)} , ${y(d.length)})` })
          .attr("width", function (d) { return x(d.x1) - x(d.x0) - 0.5 })
          .attr("height", function (d) { return height - y(d.length); })
          .style("fill", "#fe9922")

        // Add brushing
        svg.append("g")
          .call(d3.brushX()                     // Add the brush feature using the d3.brush function
            .on("end brush", brushed)
            .on("start brush", brushUpdate)
            .extent([[0, 0], [width, height]])        // initialise the brush area: start at 0,0 and finishes at width,height: it means I select the whole graph area
          ).call(d3.brush()
            .move, [[0, 0], [width, height]]
          )

        function brushUpdate() {
          var sel = d3.brushSelection(this);
          bound = [12000000 * (sel[0] / width), 12000000 * (sel[1] / width)];
          svg.select("#l")
            .attr("x", width * (bound[0] / 12000000) - (Math.round(bound[0]).toString().length) * 3.75)
            .text(bound[0] >= 12000000 ? "12000001+" : Math.round(bound[0]))
            .style('visibility', 'visible');
          svg.select("#r")
            .attr("x", width * (bound[1] / 12000000) - (Math.round(bound[1]).toString().length) * 3.75)
            .text(bound[1] >= 12000000 ? "12000001+" : Math.round(bound[1]))
            .style('visibility', 'visible');
        }
        function brushed() {
          svg.select("#l").style('visibility', 'hidden');
          svg.select("#r").style('visibility', 'hidden');
          bound[0] >= 12000000 || bound[1] >= 12000000 ? read(Math.round(bound[0]), 20000000) : read(Math.round(bound[0]), Math.round(bound[1]))
        }
        function read(l, r) {
          var s = []
          for (var i in data) {
            data[i]['price'] >= l && data[i]['price'] <= r ? s.push(data[i]['price']) : console.log();
          }
          console.log(s);
        }
      });
      function getData(s) {
        switch (s) {
          case "d":
            return testData;
          case "d1":
            return testData1;
          case "d2":
            return testData2;
          default:
            return;
        }
      }
    })
  }

  render() {
    // const styles = {
    //   labelStyle1: {
    //     color: 'blue',
    //     fontSize: '12px',
    //     position: "fixed",
    //     height: '14px',
    //     left: getPosition(width * (bound[0] / 12000000) + (40 - bound[0].toString().length * 3.75)),
    //   },
    //   labelStyle2: {
    //     color: 'blue',
    //     fontSize: '12px',
    //     position: "fixed",
    //     height: '14px',
    //     left: getPosition(width * (bound[1] / 12000000) + (40 - bound[1].toString().length * 3.75)),

    //   }
    // }

    // function getPosition(x) {
    //   // console.log(x);
    //   return String(x) + "px";
    // }
    return (
      <div id="my_dataviz">
        <div id="dataButton">
          <input type="radio" name="dataButton" value="d" />data1<br />
          <input type="radio" name="dataButton" value="d1" />data2<br />
          <input type="radio" name="dataButton" value="d2" />data3<br />
        </div>
      </div>
    );
  }
}
export default App;
